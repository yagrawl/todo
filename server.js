const express = require('express');
const bodyParser = require('body-parser');
const _ = require('underscore');
const app = express();
const PORT = process.env.PORT || 3000;

var todos = [];
var todoNextId = 1;

app.use(bodyParser.json());

app.get('/', function(req, res) {
    res.send('Todo api route');
});

app.get('/todos', function(req, res) {
    var queryParams = req.query;
    var filteredTodos = todos;

    if(queryParams.hasOwnProperty('completed') && queryParams.completed === 'true') {
        filteredTodos = _.where(filteredTodos, {completed: true});
    } else if(queryParams.hasOwnProperty('completed') && queryParams.completed === 'false') {
        filteredTodos = _.where(filteredTodos, {completed: false});
    }

    if(queryParams.hasOwnProperty('q') && queryParams.q.length > 0) {
        filteredTodos = _.filter(filteredTodos, function(todo) {
            if(todo.description.indexOf(queryParams.q) !== -1) {
                return todo;
            }
        });
    }

    res.json(filteredTodos);
});

app.get('/todos/:id', function(req, res) {
    var todoid = parseInt(req.params.id);
    var match = _.findWhere(todos, {id: todoid});
    if(match) {
        res.json(match);
    } else {
        res.status(404).send();
    }
});

app.post('/todos', function(req, res) {
    var body = req.body;

    if(!_.isString(body.description)){
        return res.status(400).send();
    }

    body.id = todoNextId++;
    todos.push(body);
    res.status(200).send();
});

app.delete('/todos/:id', function(req, res) {
    var todoid = parseInt(req.params.id);
    var match = _.findWhere(todos, {id: todoid});
    if(!match) {
        return res.status(404).send();
    } else {
        var newTodo = _.without(todos, match);
        res.json(newTodo);
    }
});

app.put('/todos/:id', function(req, res) {
    var todoid = parseInt(req.params.id);
    var match = _.findWhere(todos, {id: todoid});
    var body = _.pick(req.body, 'description', 'completed');
    var validAttributes = {};

    if(!match) {
        return res.status(404).send();
    }

    if(body.hasOwnProperty('completed') && _.isBoolean(body.completed)) {
        validAttributes.completed = body.completed;
    } else if(body.hasOwnProperty('completed')) {
        return res.status(400).send();
    }

    if(body.hasOwnProperty('description') && _.isString(body.description) && body.description.trim().length > 0) {
        validAttributes.description = body.description;
    } else if(body.hasOwnProperty('description')) {
        return res.status(400).send();
    }

    _.extend(match, validAttributes);
    res.json(match);

});

app.listen(PORT, function() {
    console.log('Listening on port: ' + PORT);
});
